﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_para_el_examen
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            do
            {
                string A = String.Empty;
                Console.WriteLine("Menu de opciones del ejercicio\n" +
                    "[1] Sistema de reservaciones para una aerolinea\n" +
                    "[2] Tabla de 4 filas por 5 columnas de elementos enteros\n" +
                    "[3] Salida del programa\n" +
                    "\n" +
                    "Anote una de las opciones anteriores");
                A = Console.ReadLine();
                switch (A)
                {
                    case "1":
                        ejercicios.programa_1();
                        a = a + 1;
                        break;
                    case "2":
                        ejercicios.programa_2();
                        a = a + 1;
                        break;
                    case "3":
                        a = a - 2;
                        break;
                    default:
                        Console.WriteLine("Esta opcion no esta permitida");
                        break;
                }
            } while (a >= 1);
            Console.WriteLine("Usted ha salido del programa");
        }

    }
}

