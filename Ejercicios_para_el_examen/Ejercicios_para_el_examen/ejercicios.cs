﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_para_el_examen
{
    class ejercicios
    {
        public static void programa_1()
        {
            System.Console.Clear();
            Console.WriteLine("Sistema de reservaciones para una aerolinea");
            int[] asientos = new int[10];
            int turnos = 0;
            for (int i = 0; i < 10; i++)
            {
               asientos[i] = 0;
               Console.WriteLine("\n" +
              "Menu de alternativas\n" +
              "[1] Primera clase\n" +
              "[2] Economico\n" +
              "Elija una de las alternativas");
                string opcion = String.Empty;
                opcion = Console.ReadLine();
                switch (opcion)
                {
                    case "1":
                        if (asientos[i] == 0 && i < 5)
                        {
                            asientos[i] = 1;
                            turnos = turnos + 1;
                            Console.WriteLine("Su numero de asiento es: " + turnos);
                            Console.WriteLine("Seccion: Primera clase");
                        }
                        else
                        {
                            Console.WriteLine("Esta categoria esta llena le podemos asignar una asiento \n" +
                                "en la clase economica o el proximo vuelo con esete destinos parte \n" +
                                "dentro de 3 horas");
                        }
                        break;
                    case "2":
                        if (asientos[i] == 0 && i < 10)
                        {
                            asientos[i] = 1;
                            turnos = turnos + 1;
                            Console.WriteLine("Su numero de asiento es: " + turnos);
                            Console.WriteLine("Su seccion: Clase economica");
                        }
                        else
                        {
                            Console.WriteLine("Esta categoria esta llena le podemos asignar una asiento \n" +
                                "en la clase economica o el proximo vuelo con esete destinos parte \n" +
                                "dentro de 3 horas");
                        }
                        break;
                    default:
                        Console.WriteLine("Esta opcion no se encuentra disponible");
                        break;
                }
            }
        }
        

        public static void programa_2()
        {
            System.Console.Clear();
            Console.WriteLine("Introducir 20 elemeentos enteros");
            Console.WriteLine("");
            int[,] numeros = new int[4, 5];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.WriteLine("Anota un numero " + i);
                    numeros[i,j] = int.Parse(Console.ReadLine());
                }
            }
            
        }
    }
}
